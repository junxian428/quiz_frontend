import { createRouter, createWebHashHistory } from 'vue-router';
import Login from '../views/Login.vue';
import VueCookies from 'vue-cookies';


const routes = [

  {
    path: '/home',
    name: 'Home',
    component: () => import(/* webpackChunkName: "about" */ '../views/HomeView.vue'),
    meta: { requiresAuth: true },
    beforeEnter: (to, from, next) => {
      const token = VueCookies.get('access_token');

      if (token === null || token === "undefined") {
        next('/'); // Redirect to the login page
      } else {
        next(); // Proceed to the protected route
      }
    },
  },

  {
    path: '/Quiz',
    name: 'quiz',
    component: () => import(/* webpackChunkName: "about" */ '../views/QuizView.vue'),
    meta: {
      requiresAuth: true, // Add the 'requiresAuth' meta field to the route
    },
  },

  {
    path: '/login',
    name: 'login',
    component: Login,
  },

  {
    path: '/Testing2',
    name: 'testing2',
    component: () => import(/* webpackChunkName: "about" */ '../views/Testing2.vue'),
  },
    // Add a catch-all route at the end
  // Add a catch-all route at the end

  {
    path: '/',
    name: 'login',
    component: Login,
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

router.beforeEach((to, from, next) => {
  const token = VueCookies.get('access_token')

  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (token === undefined) {
      // Access token is undefined, redirect to login page
      next('/login')
    } else {
      next()
    }
  } else {
    next()
  }
})


export default router;
