npm install vue-cookies

npm install axios

npm run serve

npm install --save vue-plugin-load-script@^2.x.x

npm install -D sass-loader@^10 sass

npm install bootstrap-vue bootstrap



CREATE TABLE questions (
    ID INT PRIMARY KEY AUTO_INCREMENT,
    Question TEXT,
    OptionA TEXT,
    OptionB TEXT,
    OptionC TEXT,
    OptionD TEXT,
    CorrectOption CHAR(1)
);


CREATE TABLE questions (
    ID INT PRIMARY KEY AUTO_INCREMENT,
    Question TEXT,
    OptionA TEXT,
    OptionB TEXT,
    OptionC TEXT,
    OptionD TEXT,
    CorrectOption CHAR(1)
);

INSERT INTO questions (Question, OptionA, OptionB, OptionC, OptionD, CorrectOption)
VALUES 
('What is the capital of France?', 'Paris', 'Berlin', 'Madrid', 'Rome', 'A');

INSERT INTO questions (Question, OptionA, OptionB, OptionC, OptionD, CorrectOption)
VALUES 
('What is the largest planet in our solar system?', 'Jupiter', 'Saturn', 'Neptune', 'Mars', 'A');


INSERT INTO questions (Question, OptionA, OptionB, OptionC, OptionD, CorrectOption)
VALUES 
('Who wrote the play "Romeo and Juliet"?', 'William Shakespeare', 'Charles Dickens', 'Jane Austen', 'Mark Twain', 'A');

INSERT INTO questions (Question, OptionA, OptionB, OptionC, OptionD, CorrectOption)
VALUES 
('What is the chemical symbol for water?', 'H2O', 'CO2', 'O2', 'N2', 'A');


CREATE TABLE currentuserquestion (
    id INT PRIMARY KEY AUTO_INCREMENT,
    userid INT,
    questionid INT,
    FOREIGN KEY (userid) REFERENCES _user(id),
    FOREIGN KEY (questionid) REFERENCES questions(ID)
);


CREATE TABLE useranswer (
    userid INT,
    questionid INT,
    answer CHAR(1),
    FOREIGN KEY (userid) REFERENCES _user(id),
    FOREIGN KEY (questionid) REFERENCES questions(ID)
);


CREATE TABLE usermark (
    userid INT,
    totalmark INT,
    FOREIGN KEY (userid) REFERENCES _user(id)
);
